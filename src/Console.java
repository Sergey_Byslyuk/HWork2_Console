import java.util.Scanner;

public class Console {

    public static final int SELECTIONSORTING = 1;
    public static final int BUBBLESORTING = 2;

    public static void main(String[] args) {
        Console console = new Console();
        console.runTheConsole();

    }

    private void runTheConsole() {
        int length = lengthArray();
        int array[] = new int[length];
        System.out.println("Filling array!");
        for (int i = 0; i < array.length; i++) {
            array[i] = arrayValues(length);
        }
        sorting(array);
    }

    private void sorting(int[] array) {
        System.out.println("Choose method sorting:");
        System.out.println("1.Selection sorting");
        System.out.println("2.Bubble sorting");
        Scanner scanner = new Scanner(System.in);
        int action = scanner.nextInt();
        switch (action){
            case SELECTIONSORTING:
                selectionSorting(array);
                print(array);
                break;
            case BUBBLESORTING:
                bubbleSorting(array);
                print(array);
                break;
            default:
                System.out.println("Invalid input!");
                sorting(array);
        }
    }

    private void print(int[] array) {
        System.out.println();
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }

    private int[] bubbleSorting(int[] array) {
        System.out.print("Bubble sorting: ");
        for (int i = 0; i < array.length-1; i++) {
            for (int j = 0; j < array.length-1; j++) {
                if(array[j]>array[j+1]){
                    int k = array[j+1];
                    array[j+1] = array[j];
                    array[j] = k;
                }
            }
        }
        return array;
    }

    private int[] selectionSorting(int[] array) {
        System.out.println("Selection sorting: ");
        for (int i = 0; i < array.length-1; i++) {
            int x = array[i];
            int c;
            for (int j = i+1; j < array.length; j++) {
                int min = array[j];
                if(min<x){
                    c = j;
                    array[i]= array[j];
                    array[c] = x;
                }
                x = array[i];
            }
        }
       return array;
    }

    private int arrayValues(int length) {
        System.out.println("Enter value:");
        Scanner scanner = new Scanner(System.in);
                if(scanner.hasNextInt()){
                   int value = scanner.nextInt();
                    return value;
                }
        System.out.println("Invalid value");
        return arrayValues(length);
            }

    private int  lengthArray() {
        System.out.println("Enter length of array:");
        Scanner scanner = new Scanner(System.in);
        if(scanner.hasNextInt()){
            int y = scanner.nextInt();
            return y;
        }
        else {
            System.out.println("Invalid value");
        }
        return lengthArray();
    }
}
